import React from 'react'
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import img from './hi.png'

import './Home.css'
import { BorderBottom } from '@mui/icons-material';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: 'transperant',
    color: 'black',
    fontSize: 'large',
    border: 'none',
    borderBottom: '1px solid'
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  
    backgroundColor: theme.palette.action.hover,
  
  '&': {
    backgroundColor: 'transperant',
    marginBottom: theme.spacing(1),
    borderRadius: 10,
    '& td': {
      backgroundColor: theme.palette.background.paper,
      borderRadius: 20,
      padding: theme.spacing(2),
    },
  },
  // hide last border
  // '&:last-child td, &:last-child th': {
  //   border: 0,
  // },
}));

function createData(name, DateTime, Status) {
  return { name, DateTime, Status };
}

const rows = [
  createData('No Parking', '12/12/12 12pm', 'pending'),
  createData('No Parking', '12/12/12 12pm', 'pending'),
  // createData('No Parking', '12/12/12 12pm', 'pending'),
];

function Home() {
  return (
    <div className='home-cont'>
      <h2>Welcome Back,</h2>
      <h1>Harsha Vardhna</h1>
      <div className="cont">
        <div className="cont-left">
          <div className="report">
            <div className="title">
              <h4>Your Reports</h4>
              <a href="/"><span>View More</span><ChevronRightIcon/> </a>
            </div>
            <div className="line"></div>
            <div className="table">
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 700 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell>Cause</StyledTableCell>
                      <StyledTableCell align="right">DateTime</StyledTableCell>
                      <StyledTableCell align="right">Status</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => (
                      <StyledTableRow key={row.name}>
                        {/* <StyledTableCell component="th" scope="row">
                          {row.name}
                        </StyledTableCell> */}
                        <StyledTableCell align="left">{row.name}</StyledTableCell>
                        <StyledTableCell align="right">{row.DateTime}</StyledTableCell>
                        <StyledTableCell align="right">{row.Status}</StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </div>
          <div className="pending">
            <div className="title">
            <h4>Your Challans</h4>
            <a href="/"><span>View More</span><ChevronRightIcon/> </a>
            </div>
            <div className="line"></div>
            <div className="content">
              <p><img src={img}/><span><b>Hurry!!</b>You have NO Challans</span></p>
            </div>
          </div>
        </div>
        <div className="cont-right">
          <div className="points">
            <div className="po">
              <h2>Points</h2>
              <a href="/"><span>History</span><ChevronRightIcon/> </a>
            </div>
            <h1>285-P</h1>
          </div>
          <div className="vechiles">
            <div className="title">
              <h4>Your Vechiles</h4>
              <a href="/"><span>Manage</span><ChevronRightIcon/> </a>
            </div>
            <div className="line"></div>
            <div className="num">
              <div className="vec-num">
                <ChevronRightIcon/><span>TS04E2222</span>
              </div>
              <div className="vec-num">
                <ChevronRightIcon/><span>TS04E2222</span>
              </div>
              <div className="vec-num">
                <ChevronRightIcon/><span>TS04E2222</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Home